<?php

class Router
{
    /** @var array  */
    private $routes =  [];

    /**
     * @param array $routes
     */
    public function __construct(array $routes)
    {
        $this->setRoutes($routes);
    }

    /**
     * @param array $routes
     */
    public function setRoutes(array $routes)
    {
        $this->routes = $routes;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function setRoute(string $key, string $value)
    {
        $this->routes[$key] =  $value;
    }

    /**
     * @param string $uri
     * @return bool
     */
    public function routeExists(string $uri)
    {
        return array_key_exists($uri, $this->routes);
    }

    /**
     * Gets the Key from the Dependency Injection Container.
     * @param string $uri
     * @return string
     * @throws \ErrorException
     */
    public function getDIKey(string $uri)
    {
        if ($this->routeExists($uri)) {
            return $this->routes[$uri];
        }
        throw new \ErrorException("The route {$uri} is not defined");
    }
}