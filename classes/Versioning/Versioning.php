<?php

class Versioning
{
    /**
     * @param string $url
     * @return string
     */
    public static function autoVersion(string $url) : string
    {
        $path = pathinfo($url);
        $version = '.' . filemtime(ROOT . '/' . $url) . '.';
        return $path['dirname'] . '/' . str_replace('.', $version, $path['basename']);
    }
}