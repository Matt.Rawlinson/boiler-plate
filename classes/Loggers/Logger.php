<?php

use Config;

abstract class Logger
{
    /** @var string  */
    const LOG_DIR = '';

    /** @var string */
    private $log;

    public function __construct()
    {
        $date = new \DateTimeImmutable();
        $this->log = Config::get('logPath') . static::LOG_DIR . $date->format('Y-W') . '.log';
    }

    /**
     *   @void
     *	Creates the log
     *
     *   @param string $message the message which is written into the log.
     *	@description:
     *	 1. Checks if directory exists, if not, create one and call this method again.
     *	 2. Checks if log already exists.
     *	 3. If not, new log gets created. Log is written into the logs folder.
     *	 4. Logname is current date(Year - Month - Day).
     *	 5. If log exists, edit method called.
     *	 6. Edit method modifies the current log.
     */
    public function write(string $message) {
        $date = new \DateTimeImmutable();

        if (is_dir(dirname($this->log))) {
            $fh  = fopen($this->log, 'a');
            $logContent = "[ DATE & TIME : " . $date->format('Y-m-d H:i:s')." ]\r\n" . $message ."\r\n";
            fwrite($fh, $logContent);
            fclose($fh);
        } else {
            if(mkdir(dirname($this->log),0777, true) === true)
            {
                $this->write($message);
            }
        }
    }
}