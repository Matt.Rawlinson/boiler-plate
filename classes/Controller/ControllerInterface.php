<?php

interface ControllerInterface
{
    /**
     * @return mixed
     */
    public function indexAction();
}