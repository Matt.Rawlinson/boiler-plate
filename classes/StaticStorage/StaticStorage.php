<?php

abstract class StaticStorage
{
    /** @var array  */
    protected static $storage = [];

    /**
     * @param string $key The key of the config value to get
     * @param null $default
     * @return mixed the config value or null if not found
     */
    public static function get(string $key, $default = null)
    {
        if (! is_string($key) || ! strlen($key)) {
            throw new \InvalidArgumentException('Invalid key');
        }
        if (isset(static::$storage[$key])) {
            return static::$storage[$key];
        }
        return $default;
    }

    /**
     * @param string $key The key of the value to set
     * @param mixed $value the value to set
     */
    public static function set(string $key, $value)
    {
        if (! is_string($key) || ! strlen($key)) {
            throw new \InvalidArgumentException('Invalid key');
        }
        static::$storage[$key] = $value;
    }

    /**
     * @return array
     */
    public static function getAll()
    {
        return static::$storage;

    }
}