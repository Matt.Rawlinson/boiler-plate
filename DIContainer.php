<?php

use Pimple\Container;
use Config;

require_once 'vendor/autoload.php';
require_once 'config/initialise.php';

$container = new Container();

$container['Router'] = function() {
    return new Router(Config::get('routes'));
};
