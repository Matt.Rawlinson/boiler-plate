<?php

use Config;

/** Setting path where log files will be stored */
Config::set('logPath', ROOT . '/logs/');

date_default_timezone_set('Europe/London');

include_once 'Routes.php';