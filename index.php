<?php

use ControllerInterface;
use Router;

define('ROOT', __DIR__);

require_once "autoloader.php";
require_once "DIContainer.php";

function getCurrentUri()
{
    $basePath = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';
    $uri = substr($_SERVER['REQUEST_URI'], strlen($basePath));
    if (strstr($uri, '?')) {
        $uri = substr($uri, 0, strpos($uri, '?'));
    }
    $uri = '/' . $uri;
    return $uri;
}

/** @var Router $router */
$router = $container['Router'];

if (isset($_SESSION['username'])) {
    $thisFile = basename(__FILE__, '.php');
    $controllerKey = str_replace('/'. $thisFile .'.php', '', getCurrentUri());
    $controllerDIKey =  $router->getDIKey($controllerKey);
} else {
    $controllerDIKey = 'LoginController';
}

/** @var ControllerInterface $controller */
$controller = $container[$controllerDIKey];
print $controller->indexAction();